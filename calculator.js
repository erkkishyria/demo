document.getElementById("one").addEventListener('click', event => {
    //lisätään ruudulle ykkönen
    addToScreen("1");
});

document.getElementById("two").addEventListener('click', event => {
    //lisätään ruudulle kakkonen
    addToScreen("2");
});

document.getElementById("plus").addEventListener('click', event => {
    //lisätään ruudulle kakkonen
    addToScreen("+");
});

document.getElementById("equals").addEventListener('click', event => {
    //lisätään ruudulle kakkonen
    document.getElementById("screen").value = 
    eval(document.getElementById("screen").value);
});

/**
 * Lisää parametrin mukaisen tekstin laskimen ruudulle. 
 * 
 * @param {string} smth ruudulle lisättävä teksti
 */
function addToScreen(smth) {
    document.getElementById("screen").value = 
    document.getElementById("screen").value + smth;
}



