document.getElementById("nappi").addEventListener("click", clicked);

/**
 * nappia painamalla lisätään uusia rivejä kauppalistaan, 
 * joka näkyy verkkosivulla. 
 */
function clicked() {
    console.log("Olen napin tapahtumakäsittelijässä");
    let lista = document.getElementById("lista");

    let ostettavaAsia = document.getElementById('asia').value;
    var entry = document.createElement('li');
    entry.appendChild(document.createTextNode(ostettavaAsia));
    lista.appendChild(entry);
}